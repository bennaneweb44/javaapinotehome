package fr.azizbennane.javaapinotehome.integration.category;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import fr.azizbennane.javaapinotehome.integration.EntryPointControllerTest;
import fr.azizbennane.javaapinotehome.tools.URIConstant;

@SpringBootTest
public class CategoryControllerTest extends EntryPointControllerTest {

	@Override
    protected void setUp() {
    	super.setUp();
    }
	
	/**********************************************************************************************
     ******************************************** USER ******************************************** 
     **********************************************************************************************/

	@Test
    public void userCanAccessToLoginForm() throws Exception {
    	this.mockMvc
            .perform(get(URIConstant.GET_CATEGORIES_URL)
    		.with(httpBasic("user", "user")))
            .andExpect(status().isFound())
            .andExpect(unauthenticated());
    }

    @Test
    public void userCanGetAllCategories() throws Exception {
    	this.mockMvc
		.perform(MockMvcRequestBuilders.get(URIConstant.GET_CATEGORIES_URL)
		.with(user("user").roles("USER"))
        .with(csrf())
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk());
    }
    
    @Test
    public void userCanCreateNewCategory() throws Exception {
    	this.mockMvc
			.perform(MockMvcRequestBuilders.post(URIConstant.GET_CATEGORIES_URL)
			.with(user("user").roles("USER"))
	        .with(csrf())
	        .content("{\n"
	        		+ "	\"nom\": \"nom\",\n"
	        		+ "	\"couleur\": \"couleur\",\n"
	        		+ "	\"icone\": \"icone\"\n"
	        		+ "}")
	        .contentType(MediaType.APPLICATION_JSON)
	        .accept(MediaType.APPLICATION_JSON))            
			.andExpect(status().isForbidden());
	}
    
    /**********************************************************************************************
     ******************************************* ADMIN ******************************************** 
     **********************************************************************************************/
    
    @Test
    public void adminCanAccessToLoginForm() throws Exception {
    	this.mockMvc
            .perform(get(URIConstant.GET_CATEGORIES_URL)
    		.with(httpBasic("admin", "admin")))
            .andExpect(status().isFound())
            .andExpect(unauthenticated());
    }

    @Test
    public void adminCanReadAllCategories() throws Exception {
    	this.mockMvc
			.perform(MockMvcRequestBuilders.get(URIConstant.GET_CATEGORIES_URL)
			.with(user("admin").roles("ADMIN"))
	        .with(csrf())
	        .contentType(MediaType.APPLICATION_JSON)
	        .accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
    }
 
    @Test
    public void adminCanCreateNewCategory() throws Exception {
    	this.mockMvc
    		.perform(MockMvcRequestBuilders.post(URIConstant.GET_CATEGORIES_URL)
			.with(user("admin").roles("ADMIN"))
            .with(csrf())
            .content("{\n"
            		+ "	\"nom\": \"nom\",\n"
            		+ "	\"couleur\": \"couleur\",\n"
            		+ "	\"icone\": \"icone\"\n"
            		+ "}")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))            
			.andExpect(status().isCreated());
	}
}
