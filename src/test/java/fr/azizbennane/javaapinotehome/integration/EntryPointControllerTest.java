package fr.azizbennane.javaapinotehome.integration;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import fr.azizbennane.javaapinotehome.tools.URIConstant;
import jakarta.servlet.Filter;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@RequestMapping("/api")
@Sql("/data.sql")
public class EntryPointControllerTest {
	
	@Autowired
    private WebApplicationContext context;
    @Autowired
    private Filter springSecurityFilterChain;
    @Autowired
    protected MockMvc mockMvc;

    @BeforeEach
    protected void setUp() {
    	mockMvc = MockMvcBuilders
    			.webAppContextSetup(context)
    			.addFilters(springSecurityFilterChain)
    			.build();
    }
    
    @Test
    public void requiresAuthentication() throws Exception {
    	
    	mockMvc
            .perform(get(URIConstant.GET_HOME_PAGE_URL))
            .andExpect(status().isFound());
    }

    @Test
    public void authenticationAdminSuccess() throws Exception {
    	mockMvc
	    	.perform(formLogin().user("admin").password("admin"))
	        .andExpect(status().is3xxRedirection())
	        .andExpect(redirectedUrl(URIConstant.GET_HOME_PAGE_URL))
	        .andExpect(authenticated());
    }
    
    @Test
    public void authenticationAdminFailed() throws Exception {
    	mockMvc
            .perform(formLogin().user("admin").password("invalid"))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl(URIConstant.GET_LOGIN_URL + "?error"))
            .andExpect(unauthenticated());
    }
    
    @Test
    public void authenticationUserSuccess() throws Exception {
    	mockMvc
	    	.perform(formLogin().user("user").password("user"))
	        .andExpect(status().is3xxRedirection())
	        .andExpect(redirectedUrl(URIConstant.GET_HOME_PAGE_URL))
	        .andExpect(authenticated());
    }

    @Test
    public void authenticationUserFailed() throws Exception {
    	mockMvc
            .perform(formLogin().user("user").password("invalid"))
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl(URIConstant.GET_LOGIN_URL + "?error"))
            .andExpect(unauthenticated());
    }
}
