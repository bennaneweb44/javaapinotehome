package fr.azizbennane.javaapinotehome.unit;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import fr.azizbennane.javaapinotehome.controller.CategoryController;
import fr.azizbennane.javaapinotehome.service.CategoryService;

@WebMvcTest(controllers = CategoryController.class)
public class CategoryControllerTest {
	
	@Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryService categoryService;

    @Test
    public void testGetCategories() throws Exception {
        mockMvc.perform(get("/api/categories"))
            .andExpect(status().isUnauthorized());
    }
}

