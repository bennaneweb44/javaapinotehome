-- Users
CREATE TABLE IF NOT EXISTS user (
  id int NOT NULL AUTO_INCREMENT,
  username varchar(255) DEFAULT NULL,
  password varchar(255) DEFAULT NULL,
  roles varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);
DELETE from user;
INSERT INTO user (username, password, roles) VALUES 
('user', '$2y$10$.qkbukzzX21D.bqbI.B2R.tvWP90o/Y16QRWVLodw51BHft7ZWbc.', 'USER'),
('admin', '$2y$10$kp1V7UYDEWn17WSK16UcmOnFd1mPFVF6UkLrOOCGtf24HOYt8p1iC', 'ADMIN');

-- categories
CREATE TABLE IF NOT EXISTS category (
  id int NOT NULL AUTO_INCREMENT,
  couleur varchar(255) DEFAULT NULL,
  icone varchar(255) DEFAULT NULL,
  nom varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);
DELETE from category;
INSERT INTO category (nom, couleur, icone) VALUES
('rouge', '#ff0000', 'icone-rouge'),
('bleu', '#00ff00', 'icone-bleu'),
('vert', '#0000ff', 'icone-vert');

