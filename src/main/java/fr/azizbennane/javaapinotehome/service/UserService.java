package fr.azizbennane.javaapinotehome.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import fr.azizbennane.javaapinotehome.model.UserEntity;
import fr.azizbennane.javaapinotehome.repository.UserRepository;
import lombok.Data;

@Data
@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public Iterable<UserEntity> getUsers() {
		return userRepository.findAll();
	}
}
