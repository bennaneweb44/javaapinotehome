package fr.azizbennane.javaapinotehome.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.azizbennane.javaapinotehome.model.Category;
import fr.azizbennane.javaapinotehome.repository.CategoryRepository;

import lombok.Data;

@Data
@Service
public class CategoryService {
	
	@Autowired
    private CategoryRepository categoryRepository;

    public Optional<Category> getCategories(final Long id) {
        return categoryRepository.findById(id);
    }

    public Iterable<Category> getCategories() {
        return categoryRepository.findAll();
    }

    public void deleteCategory(final Long id) {
    	categoryRepository.deleteById(id);
    }

    public Category saveCategory(Category category) {
    	Category savedCategory = categoryRepository.save(category);
        return savedCategory;
    }
}
