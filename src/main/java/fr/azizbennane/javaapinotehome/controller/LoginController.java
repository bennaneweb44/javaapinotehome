package fr.azizbennane.javaapinotehome.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

	@GetMapping("/user")
	public String getUser() {
		return "Bienvenue, User";
	}
	
	@GetMapping("/admin")
	public String getAdmin() {
		return "Bienvenue, Admin";
	}
}
