package fr.azizbennane.javaapinotehome.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.azizbennane.javaapinotehome.exception.ResourceNotFoundException;
import fr.azizbennane.javaapinotehome.model.Category;
import fr.azizbennane.javaapinotehome.service.CategoryService;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/api")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/categories/{id}")
    public Category getCategoryById(@PathVariable(value = "id") Long categoryId) {
        return categoryService.getCategories(categoryId)
                .orElseThrow(() -> new ResourceNotFoundException("Category", "id", categoryId));
    }
    
    @GetMapping("/categories")
    public Iterable<Category> getCategories() {
        return categoryService.getCategories();
    }
    
    @PostMapping("/categories")
    @ResponseStatus(HttpStatus.CREATED)
    public Category postCategory(@RequestBody Category category) {    	
    	categoryService.saveCategory(category);
    	return category;
   }
}
