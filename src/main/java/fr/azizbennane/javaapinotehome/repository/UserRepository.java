package fr.azizbennane.javaapinotehome.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import fr.azizbennane.javaapinotehome.model.UserEntity;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long> {
	public UserEntity findByUsername(String username);
}
