package fr.azizbennane.javaapinotehome.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.azizbennane.javaapinotehome.model.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

}
