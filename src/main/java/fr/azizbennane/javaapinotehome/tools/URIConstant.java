package fr.azizbennane.javaapinotehome.tools;

import org.springframework.beans.factory.annotation.Value;

public class URIConstant {
	
	@Value("${api.prefix}")
    private static final String API_PREFIX = "/api";

	public static final String GET_HOME_PAGE_URL = "/";
	public static final String GET_LOGIN_URL = "/login";
	public static final String GET_CATEGORIES_URL = API_PREFIX + "/categories";
	public static final String GET_USERS_URL = API_PREFIX + "/users";
	public static final String GET_NOTES_URL = API_PREFIX + "/notes";
}
